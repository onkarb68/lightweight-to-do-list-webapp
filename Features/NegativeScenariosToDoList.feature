Feature: Verify positive scenarios of lightweight to do list web application

  Scenario: Verify user is not able to create the blank(with spaces) to-do item in to-do-list
    Given User has navigated to to-do-list page for negative testing
    When User tries to create item by entering multiple spaces followed by enter button
    Then User should not be able to see any item in to-do-list

  Scenario Outline: Verify user is able to create the to-do item without any character limit(2000) in to-do-list
    Given User has navigated to to-do-list page for negative testing
    When User tries to create "<2000 character to-do-item>"
    Then Verify user is able to create "<2000 character to-do-item>" the to-do item without any character limit

    Examples: 
      | 2000 character to-do-item                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        |
      | 11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 |

  #As below user case is defect, so its failing. Need to create jira defect for it
  Scenario Outline: [Defect]Verify creation of same to-do-list in the to do list
    Given User has navigated to to-do-list page for negative testing
    When User tries to create exactly same "<to-do-item>" in todolist
    Then User should not able be create exactly same "<to-do-item>" again,as it will create confusion

    Examples: 
      | to-do-item |
      | Bath       |

  Scenario Outline: Verify creation of same to-do-list in the to do list
    Given User has navigated to to-do-list page for negative testing
    When User adds item having only "<alphabets>" containing only uppercase, lowercase , both upper and lower case
    Then User should be able to add item having "<alphabets>" containing only uppercase, lowercase , both upper and lower case
    When User adds item having "<alphanumeric>" characters
    Then User should be able to add item having "<alphanumeric>" characters
    When User adds item having only "<special characters>"
    Then User should be add item having "<special characters>"

    Examples: 
      | alphabets | alphanumeric        | special characters |
      | bath      | bath at 6AM         | bath @ 6AM         |
      | Breakfast | Breakfast at 10  AM | Breakfast @ 10  AM |
      | LUNCH     | LUNCH at 1 PM       | LUNCH @ 1 PM       |

  Scenario Outline: Verify user is able to create large number(10) to-do items without any limitation and performanace issue
    Given User has navigated to to-do-list page for negative testing
    When User tries to create "<repetative Item>" 10 times
    Then Verify user is able to create "<repetative Item>" unlimited to-do items 10 times without any limitation and performanace issue

    Examples: 
      | repetative Item                    |
      | Create large number of to-do-items |
