Feature: Verify positive scenarios of lightweight to do list web application

  Scenario: Verify user is able to create the single to-do item in to-do-list
    Given User has navigated to to-do-list page for positive testing
    When User enters "Jogging" in the placeholder for adding it as a item
    Then User should be able to see "Jogging" as one of the item in to-do-list

  Scenario: Verify user is able to add multiple to-do items in to-do-list
    Given User has navigated to to-do-list page for positive testing
    When User adds multiple ToDoItems in list of to-do-items
      | Gymnasium    |
      | Bath         |
      | Breakfast    |
      | GettingReady |
    Then User should be able to see ToDoItem as one of the item in to-do-list
      | Gymnasium    |
      | Bath         |
      | Breakfast    |
      | GettingReady |

  Scenario: Verify total of to-do-list item count gets incremented by one when single to-do items in to-do-list gets added
    Given User has navigated to to-do-list page for positive testing
    When User enters single ToDoItem one by one as a on of the list of items
      | Gymnasium    |
      | Bath         |
      | Breakfast    |
      | GettingReady |
    Then Verify total to-do-list item count gets incremented by one when single to-do-list gets added

  Scenario: Verify user is able to add & complete multiple of to-do items in to-do-list
    Given User has navigated to to-do-list page for positive testing
    When User adds multiple ToDoItems in list of to-do-items & complets all the to-do-lists tasks one by one
      | Gymnasium    |
      | Bath         |
      | Breakfast    |
      | GettingReady |
    Then Verify total to-do-list item count gets decremented by one each time when user single complets task

  Scenario: Verify user is able to delete multiple to-do items from to-do-list without completing it.
    Given User has navigated to to-do-list page for positive testing
    When User adds multiple ToDoItems in list of to-do-items & deletes all the to-do-lists tasks one by one
      | Gymnasium    |
      | Bath         |
      | Breakfast    |
      | GettingReady |
    Then All added items should get deleted
