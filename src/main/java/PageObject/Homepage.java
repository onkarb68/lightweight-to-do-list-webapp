package PageObject;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import Utility.BaseClass;

/**
 * @author ${Onkar Bhagwat}
 *
 * 
 */
public class Homepage extends BaseClass {

	public static WebDriver driver;
	static Logger logger = Logger.getLogger("Homepage");

	public Homepage(WebDriver driver) {

		super(driver);

	}

	@FindBy(how = How.XPATH, using = "//input[@placeholder=\'What needs to be done?\']")
	public static WebElement inputTextBox;

	@FindBy(how = How.XPATH, using = "//*[text()='todos']")
	public static WebElement todosLabel;

	@FindBy(how = How.XPATH, using = "//*[starts-with(text(),' item')]/strong")
	public static WebElement totalItemsLeftCount;

	@FindBy(how = How.XPATH, using = "//div[@class=\"view\"]/input[@type=\"checkbox\"]")
	public static WebElement itemCheckbox;

	@FindBy(how = How.XPATH, using = "//button[@class=\"destroy\"]")
	public static WebElement deleteItem;

	@FindBy(how = How.XPATH, using = "//*[text()='Clear completed']")
	public static WebElement clearCompletedMessage;

	// This method creates to-do items in list
	public void enterTodoListItem(String itemText) {
		click(inputTextBox);
		sendText(inputTextBox, itemText);
		waitForContent();
		pressEnterKey(inputTextBox);
	}

	// This method verifies created to-do item from list
	public boolean isToDoListItemDisplayed(String itemText) {
		String itemXapth = "//*[text()='" + itemText + "']";
		WebElement item = BaseClass.driver.findElement(By.xpath(itemXapth));
		getElement(item);
		return isElementDisplayed(item);

	}

	// This method verifies the count of created to-do item from list
	public String getTotalTodoListItemsCount() {

		return getText(totalItemsLeftCount);

	}

	// This method deletes created to-do item from list
	public void deleteItem(String itemText) {

		String itemXapth = "//*[text()='" + itemText + "']";
		waitForContent();
		WebElement itemName = BaseClass.driver.findElement(By.xpath(itemXapth));
		retryingClick(itemName);

		String itemXapthLocator = "//*[text()='" + itemText + "']/parent::div/button";
		WebElement item = BaseClass.driver.findElement(By.xpath(itemXapthLocator));
		item = getElement(item);
		retryingClick(item);

	}

	// This method completes created to-do item from list
	public void completeItem(String itemText) {

		String xpathItem = "//*[text()='" + itemText + "']/parent::div/input";
		waitForContent();
		WebElement completeItem = BaseClass.driver.findElement(By.xpath(xpathItem));
		retryingFindElement(completeItem);
		retryingClick(completeItem);
	}

	// This method creates blank with spaces to-do item
	public void enterBlankListItem() {
		click(inputTextBox);
		for (int i = 0; i < 10; i++) {
			inputTextBox.sendKeys(Keys.SPACE);
		}
		waitForContent();
		pressEnterKey(inputTextBox);
	}

	// This method returns the count of created to-do item for given item name
	public List<WebElement> getItemList(String itemName) {

		String xpathName = "//*[text()='" + itemName + "']";
		return getWebElements(xpathName);

	}
}
