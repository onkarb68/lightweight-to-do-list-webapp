package Utility;

import java.io.IOException;
import java.util.List;
import java.util.Properties;

import javax.annotation.Nonnull;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.ElementClickInterceptedException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BaseClass {
	static Logger logger = Logger.getLogger("BaseClass");
	public static WebDriver driver;
	public static boolean bResult;
	public static Properties prop;

	public BaseClass(WebDriver driver) {
		BaseClass.driver = driver;
		BaseClass.bResult = true;
	}

	// This method wait till entire DOM gets loaded completely
	public void waitForContent() {
		ExpectedCondition<Boolean> loaded = new ExpectedCondition<Boolean>() {
			@Override
			public Boolean apply(@Nonnull WebDriver driver) {
				String result = "return ((document.readyState === 'complete') && ($.active == 0))";
				return (Boolean) ((JavascriptExecutor) BaseClass.driver).executeScript(result);
			}
		};
		try {
			new WebDriverWait(BaseClass.driver, 100).until(loaded);
		} catch (WebDriverException e) {
			loaded = new ExpectedCondition<Boolean>() {
				@Override
				public Boolean apply(@Nonnull WebDriver driver) {
					String result = "return ((document.readyState === 'complete'))";
					return (Boolean) ((JavascriptExecutor) BaseClass.driver).executeScript(result);
				}
			};
			new WebDriverWait(BaseClass.driver, 100).until(loaded);
		}
	}

	public WebElement getElement(WebElement element) {

		WebDriverWait wait = new WebDriverWait(BaseClass.driver, 150);
		wait.until(ExpectedConditions.visibilityOf(element));
		WebElement ele = wait.until(ExpectedConditions.elementToBeClickable(element));
		return ele;
	}

	// This method to be used for the element which frequently gets changed in DOM
	// and gives StaleElementReferenceException
	public boolean retryingClick(WebElement ele) {
		boolean result = false;
		int attempts = 0;
		while (attempts < 20) {
			try {
				// waitForContent();
				retryingFindElement(ele);
				ele.click();
				result = true;
				break;
			} catch (NoSuchElementException e) {

				logger.info("Refinding the element as its throwing NoSuchElementException");

			} catch (ElementClickInterceptedException e) {

				logger.info("Refinding the element as its throwing ElementClickInterceptedException");

			} catch (StaleElementReferenceException e) {

				logger.info("Refinding the element as its throwing StaleElementReferenceException");

			} catch (TimeoutException e) {

				logger.info("Refinding the element as its throwing TimeoutException");

			} catch (Exception e) {

				logger.info("Refinding the element as its throwing exception");
			}
			attempts++;
		}
		return result;
	}

	// This method to be used for the element which frequently gets changed in DOM
	// and gives NoSuchElementException
	public boolean retryingFindElement(WebElement ele) {
		boolean result = false;
		int attempts = 0;
		while (attempts < 20) {
			try {
				// waitForContent();
				ele.isDisplayed();
				result = true;
				break;
			} catch (NoSuchElementException e) {

				logger.info("Refinding the element as its throwing NoSuchElementException");

			} catch (ElementClickInterceptedException e) {

				logger.info("Refinding the element as its throwing ElementClickInterceptedException");

			} catch (StaleElementReferenceException e) {

				logger.info("Refinding the element as its throwing StaleElementReferenceException");

			} catch (TimeoutException e) {

				logger.info("Refinding the element as its throwing TimeoutException");

			} catch (Exception e) {

				logger.info("Refinding the element as its throwing exception");
			}
			attempts++;
		}
		return result;
	}

	public void navigateToUrl(String url) {

		logger.info("Fetching the Application URL--> " + " " + url + " " + "Opening the Url");
		BaseClass.driver.navigate().to(url);

	}

	// This method gives current browser url
	public String getCurrentUrl() throws IOException {

		return BaseClass.driver.getCurrentUrl();

	}

	// This method waits till the url contains given suburl
	public boolean waitForUrlContains(String expectedUrl) {

		WebDriverWait wait = new WebDriverWait(BaseClass.driver, 5000);
		ExpectedCondition<Boolean> urlIsCorrect = arg0 -> BaseClass.driver.getCurrentUrl().contains(expectedUrl);
		return wait.until(urlIsCorrect);

	}

	// This method enters the text
	public void sendText(WebElement element, String textToEnter) {
		waitForContent();
		getElement(element);
		retryingFindElement(element);
		element.sendKeys(textToEnter);
	}

	// This method press the Enter button
	public void pressEnterKey(WebElement element) {

		getElement(element);
		element.sendKeys(Keys.ENTER);
	}

	// This method checks the display of webelement
	public boolean isElementDisplayed(WebElement element) {
		waitForContent();
		getElement(element);
		retryingFindElement(element);
		waitForContent();
		return element.isDisplayed();
	}

	// This method return the text of webelement
	public String getText(WebElement element) {
		waitForContent();
		getElement(element);
		retryingFindElement(element);
		waitForContent();
		return element.getText();
	}

	// This method click on element
	public void click(WebElement element) {
		waitForContent();
		getElement(element);
		retryingFindElement(element);
		waitForContent();
		retryingClick(element);
	}

	// This method checks absence of element
	public boolean isElementNotDisplayed(WebElement element) {

		try {
			element.isDisplayed();
			return false;
		} catch (org.openqa.selenium.NoSuchElementException e) {
			return true;
		}

	}

	// This method returns the list of webelement
	public List<WebElement> getWebElements(String xpathElement) {

		List<WebElement> elementList = BaseClass.driver.findElements(By.xpath(xpathElement));
		return elementList;

	}
}
