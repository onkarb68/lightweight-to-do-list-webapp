package StepDefination;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import PageObject.Homepage;
import TestData.HomepageData;
import Utility.BaseClass;
import Utility.Hooks;
import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

/**
 * @author ${Onkar Bhagwat}
 *
 * 
 */
public class ToDoListPositiveStepDefinition {
	static Logger logger = Logger.getLogger("ToDoListPositiveStepDefinition");

	public static boolean bResult;
	public static Properties prop;
	public static WebDriver driver;
	BaseClass baseClass;
	Homepage homepage;
	static List<String> totalListCount, clearCompletedList;

	@Given("^User has navigated to to-do-list page for positive testing$")
	public void userNavigate() throws Throwable {

		driver = Hooks.driver;
		homepage = new Homepage(driver);

		// Initiate page objects
		PageFactory.initElements(driver, Homepage.class);

		prop = new Properties();
		FileInputStream fis = new FileInputStream("src//main//resources//browser.properties");
		prop.load(fis);
		String toDoListUrl = prop.getProperty("url") + HomepageData.toDoListEndPoint;
		baseClass = new BaseClass(driver);
		baseClass.navigateToUrl(toDoListUrl);

		// Verifies User landed to correct page, url
		Assert.assertTrue(baseClass.waitForUrlContains(HomepageData.toDoListEndPoint),
				"Current url is not navigated to to do list");
		baseClass.waitForContent();
		Assert.assertTrue(baseClass.isElementDisplayed(Homepage.todosLabel),
				"Current url is not navigated to to do list");
		Assert.assertTrue(baseClass.isElementDisplayed(Homepage.inputTextBox),
				"Current url is not navigated to to do list");
		
		logger.info("Verifies User landed to correct page, url");
	}

	@When("^User enters \"([^\"]*)\" in the placeholder for adding it as a item$")
	public void userEnters(String itemName) throws Throwable {
		
		homepage.enterTodoListItem(itemName);

		logger.info("User enters itemname in the placeholder for adding it as a item");
	}

	@Then("^User should be able to see \"([^\"]*)\" as one of the item in to-do-list$")
	public void userVerification(String toDoListItem) throws Throwable {

		// Assertion to check created to-do list
		Assert.assertTrue(homepage.isToDoListItemDisplayed(toDoListItem),
				"Give Item" + toDoListItem + "is not added in to do list");
		
		logger.info("^User should be able to see itemname as one of the item in to-do-list");
	}

	@When("^User enters single ToDoItem one by one as a on of the list of items$")
	public void userEntersSingleToDoItemOneByone(DataTable toDoItemsList) throws Throwable {

		List<String> rows = toDoItemsList.asList(String.class);

		totalListCount = new ArrayList<String>();

		// User entering single to-do items one by one
		homepage.enterTodoListItem(rows.get(0));
		totalListCount.add(0, baseClass.getText(Homepage.totalItemsLeftCount));

		homepage.enterTodoListItem(rows.get(1));
		totalListCount.add(1, baseClass.getText(Homepage.totalItemsLeftCount));

		homepage.enterTodoListItem(rows.get(2));
		totalListCount.add(2, baseClass.getText(Homepage.totalItemsLeftCount));

		homepage.enterTodoListItem(rows.get(3));
		totalListCount.add(3, baseClass.getText(Homepage.totalItemsLeftCount));
		
		logger.info("User enters single ToDoItem one by one as a on of the list of items");

	}

	@Then("^Verify total to-do-list item count gets incremented by one when single to-do-list gets added$")
	public void userVerifyToDoItemCount() throws Throwable {

		// Verification for total created to-do items count which gets incremented each
		// time when new item created
		Assert.assertEquals(totalListCount.get(0), "1");
		Assert.assertEquals(totalListCount.get(1), "2");

		Assert.assertEquals(totalListCount.get(2), "3");
		Assert.assertEquals(totalListCount.get(3), "4");
		
		logger.info("Verify total to-do-list item count gets incremented by one when single to-do-list gets added");

	}

	@When("^User adds multiple ToDoItems in list of to-do-items$")
	public void userAddsMultipleItems(DataTable toDoItemsList) throws Throwable {

		List<String> rows = toDoItemsList.asList(String.class);

		// User adding multiple to-do items
		homepage.enterTodoListItem(rows.get(0));
		homepage.enterTodoListItem(rows.get(1));
		homepage.enterTodoListItem(rows.get(2));
		homepage.enterTodoListItem(rows.get(3));
		
		logger.info("User adds multiple ToDoItems in list of to-do-items");
	}

	@Then("^User should be able to see ToDoItem as one of the item in to-do-list$")
	public void userVerifiesItems(DataTable toDoItemsList) throws Throwable {
		List<String> rows = toDoItemsList.asList(String.class);

		// Verification for the creation of multiple to-do items
		Assert.assertTrue(homepage.isToDoListItemDisplayed(rows.get(0)),
				"Give Item" + rows.get(0) + "is not added in to do list");

		Assert.assertTrue(homepage.isToDoListItemDisplayed(rows.get(1)),
				"Give Item" + rows.get(1) + "is not added in to do list");

		Assert.assertTrue(homepage.isToDoListItemDisplayed(rows.get(2)),
				"Give Item" + rows.get(2) + "is not added in to do list");

		Assert.assertTrue(homepage.isToDoListItemDisplayed(rows.get(3)),
				"Give Item" + rows.get(3) + "is not added in to do list");

		Assert.assertEquals(rows.size(), Integer.parseInt(homepage.getTotalTodoListItemsCount()));
		
		logger.info("User should be able to see ToDoItem as one of the item in to-do-list");
	}

	@When("^User adds multiple ToDoItems in list of to-do-items & complets all the to-do-lists tasks one by one$")
	public void userCreatesAndCompletesTasks(DataTable toDoItemsList) throws Throwable {

		clearCompletedList = new ArrayList<String>();
		List<String> rows = toDoItemsList.asList(String.class);

		// User entering single to-do items one by one
		homepage.enterTodoListItem(rows.get(0));
		homepage.enterTodoListItem(rows.get(1));
		homepage.enterTodoListItem(rows.get(2));
		homepage.enterTodoListItem(rows.get(3));

		// User completes each to-do task one by one
		totalListCount = new ArrayList<String>();
		totalListCount.add(0, baseClass.getText(Homepage.totalItemsLeftCount));

		homepage.completeItem(rows.get(0));
		totalListCount.add(1, baseClass.getText(Homepage.totalItemsLeftCount));
		clearCompletedList.add(0, baseClass.getText(Homepage.clearCompletedMessage));

		homepage.completeItem(rows.get(1));
		totalListCount.add(2, baseClass.getText(Homepage.totalItemsLeftCount));
		clearCompletedList.add(1, baseClass.getText(Homepage.clearCompletedMessage));

		homepage.completeItem(rows.get(2));
		totalListCount.add(3, baseClass.getText(Homepage.totalItemsLeftCount));
		clearCompletedList.add(2, baseClass.getText(Homepage.clearCompletedMessage));

		homepage.completeItem(rows.get(3));
		totalListCount.add(4, baseClass.getText(Homepage.totalItemsLeftCount));
		clearCompletedList.add(3, baseClass.getText(Homepage.clearCompletedMessage));
		
		logger.info("User adds multiple ToDoItems in list of to-do-items & complets all the to-do-lists tasks one by one");
	}

	@Then("^Verify total to-do-list item count gets decremented by one each time when user single complets task$")
	public void verifyVerifiesDecrementedCount() throws Throwable {

		// Verification for total item count when user completes the task
		Assert.assertEquals(totalListCount.get(0), "4");
		Assert.assertEquals(totalListCount.get(1), "3");

		Assert.assertEquals(totalListCount.get(2), "2");
		Assert.assertEquals(totalListCount.get(3), "1");

		Assert.assertEquals(totalListCount.get(4), "0");

		for (String clearComepltedMessage : clearCompletedList) {
			Assert.assertEquals(clearComepltedMessage, HomepageData.clearComepltedMessage);
		}
		
		logger.info("Verify total to-do-list item count gets decremented by one each time when user single complets task");
	}

	@When("^User adds multiple ToDoItems in list of to-do-items & deletes all the to-do-lists tasks one by one$")
	public void userDeletesItems(DataTable toDoItemsList) throws Throwable {

		List<String> rows = toDoItemsList.asList(String.class);

		// User entering single to-do items one by one
		homepage.enterTodoListItem(rows.get(0));
		homepage.enterTodoListItem(rows.get(1));
		homepage.enterTodoListItem(rows.get(2));
		homepage.enterTodoListItem(rows.get(3));

		// User deletes single to-do items one by one
		homepage.deleteItem(rows.get(0));
		homepage.deleteItem(rows.get(1));
		homepage.deleteItem(rows.get(2));
		homepage.deleteItem(rows.get(3));
		
		logger.info("User adds multiple ToDoItems in list of to-do-items & deletes all the to-do-lists tasks one by one");

	}

	@Then("^All added items should get deleted$")
	public void userVerifiesDeletedItems() throws Throwable {

		// Verification of deletion of all the to-do item
		Assert.assertTrue(baseClass.isElementNotDisplayed(Homepage.itemCheckbox),
				"User is not able to delete the added items");
		
		logger.info("All added items should get deleted");
	}

}
