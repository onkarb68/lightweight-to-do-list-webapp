package StepDefination;

import java.io.FileInputStream;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import PageObject.Homepage;
import TestData.HomepageData;
import Utility.BaseClass;
import Utility.Hooks;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

/**
 * @author ${Onkar Bhagwat}
 *
 * 
 */
public class ToDoListNegativeStepDefinition {
	static Logger logger = Logger.getLogger("BaseClass");

	public static boolean bResult;
	public static Properties prop;
	public static WebDriver driver;
	BaseClass baseClass;
	Homepage homepage;
	static List<String> totalListCount, clearCompletedList;

	@Given("^User has navigated to to-do-list page for negative testing$")
	public void userNavigateToWebApp() throws Throwable {

		driver = Hooks.driver;
		homepage = new Homepage(driver);
		// Initiate page objects
		PageFactory.initElements(driver, Homepage.class);

		prop = new Properties();
		FileInputStream fis = new FileInputStream("src//main//resources//browser.properties");
		prop.load(fis);
		String toDoListUrl = prop.getProperty("url") + HomepageData.toDoListEndPoint;
		baseClass = new BaseClass(driver);
		baseClass.navigateToUrl(toDoListUrl);

		// Verifies User landed to correct page, url
		Assert.assertTrue(baseClass.waitForUrlContains(HomepageData.toDoListEndPoint),
				"Current url is not navigated to to do list");
		baseClass.waitForContent();
		Assert.assertTrue(baseClass.isElementDisplayed(Homepage.todosLabel),
				"Current url is not navigated to to do list");
		Assert.assertTrue(baseClass.isElementDisplayed(Homepage.inputTextBox),
				"Current url is not navigated to to do list");
		
		logger.info("Verifies User landed to correct page, url");
	}

	@When("^User tries to create item by entering multiple spaces followed by enter button$")
	public void userCreateBlankItem() throws Throwable {

		// User created blank item having multiple spaces
		homepage.enterBlankListItem();
		
		logger.info("User tries to create item by entering multiple spaces followed by enter button");
	}

	@Then("^User should not be able to see any item in to-do-list$")
	public void userVerifiesBlankItem() throws Throwable {

		// user creation of Verifies Blank Item
		Assert.assertTrue(baseClass.isElementNotDisplayed(Homepage.itemCheckbox),
				"User is able to add blank to do item,which should not be allowed");
		
		logger.info("User should not be able to see any item in to-do-list");
	}

	@When("^User tries to create \"([^\"]*)\"$")
	public void userCreateHeavyItem(String itemName) throws Throwable {

		// Creating to do item having 2000 characters in it
		homepage.enterTodoListItem(itemName);
		logger.info("User tries to create item");
	}

	@Then("^Verify user is able to create \"([^\"]*)\" the to-do item without any character limit$")
	public void verifyHeavyItem(String itemName) throws Throwable {

		// Verification of creation of item having 2000 characters in it
		Assert.assertTrue(homepage.isToDoListItemDisplayed(itemName));
		
		logger.info("Verify user is able to create to-do item without any character limit");
	}

	@When("^User tries to create exactly same \"([^\"]*)\" in todolist$")
	public void userCreateItem(String itemName) throws Throwable {
		// Write code here that turns the phrase above into concrete actions

		// User creating exact same item 5 times again and again
		for (int i = 0; i < 5; i++) {
			homepage.enterTodoListItem(itemName);
		}
		logger.info("User tries to create exactly same item in todolist");
	}

	@Then("^User should not able be create exactly same \"([^\"]*)\" again,as it will create confusion$")
	public void userVerifiesSameItem(String toDoListItem) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		List<WebElement> lengthOfItems = homepage.getItemList(toDoListItem);

		// Defect
		// This is expected failure as user should not & will not create exactly same
		// to-do-list again. If he tries to create same, then popup should be
		// displayed as "You have already created exactly same item, please check it"

		Assert.assertEquals(lengthOfItems.size(), 1);
		logger.info("User should not able be create exactly same item again,as it will create confusion");
	}

	@When("^User adds item having only \"([^\"]*)\" containing only uppercase, lowercase , both upper and lower case$")
	public void userCreatesAllTypesOfItem(String itemName) throws Throwable {

		// User Create all types of items with different characters
		homepage.enterTodoListItem(itemName);
		
		logger.info("User adds item having containing only uppercase, lowercase , both upper and lower case");
	}

	@Then("^User should be able to add item having \"([^\"]*)\" containing only uppercase, lowercase , both upper and lower case$")
	public void userVerifiesAllTypesOfItem(String itemName) throws Throwable {
		// User verifies all types of items with different characters
		Assert.assertTrue(homepage.isToDoListItemDisplayed(itemName));
		logger.info("User should be able to add tem having containing only uppercase, lowercase , both upper and lower case");
	}

	@When("^User adds item having \"([^\"]*)\" characters$")
	public void userAddsItem(String itemName) throws Throwable {
		// User add item having alphanumeric
		homepage.enterTodoListItem(itemName);
		logger.info("User adds item large no of characters");
	}

	@Then("^User should be able to add item having \"([^\"]*)\" characters$")
	public void userVerifiesItem(String itemName) throws Throwable {
		// User verifies added item having alphanumeric and special characters
		Assert.assertTrue(homepage.isToDoListItemDisplayed(itemName));
		logger.info("User should be able to add item having large characters");
	}

	@When("^User adds item having only \"([^\"]*)\"$")
	public void userAddsItemWithSpecialCharacters(String itemName) throws Throwable {
		// User add item having special characters
		homepage.enterTodoListItem(itemName);
		logger.info("User adds item");
	}

	@Then("^User should be add item having \"([^\"]*)\"$")
	public void userVerifiesItemWithSpecialCharacters(String itemName) throws Throwable {
		// User verifies item having special characters
		Assert.assertTrue(homepage.isToDoListItemDisplayed(itemName));
		logger.info("User should be add item having special characters");
	}

	@When("^User tries to create \"([^\"]*)\" (\\d+) times$")
	public void userCreatesRepetativeItem(String itemName, int counter) throws Throwable {
		// User creates repetitive item
		for (int i = 0; i < counter; i++) {
			homepage.enterTodoListItem(itemName);
		}
		logger.info("User tries to create item repetitively");
	}

	@Then("^Verify user is able to create \"([^\"]*)\" unlimited to-do items (\\d+) times without any limitation and performanace issue$")
	public void userVerifiesRepetativeItem(String itemName, int counter) throws Throwable {

		// User verifies created repetitive item
		baseClass.waitForContent();
		((JavascriptExecutor) driver).executeScript("window.scrollBy(0,500)");

		List<WebElement> lengthOfItems = homepage.getItemList(itemName);

		baseClass.waitForContent();
		Assert.assertEquals(lengthOfItems.size(), counter);
		logger.info("Verify user is able to create item unlimited to-do items times without any limitation and performanace issue");
	}

}
