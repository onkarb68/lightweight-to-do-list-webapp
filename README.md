# Getting started with https://todomvc.com/examples/angular2/ Automation testing with Cucumber , Extent Report & Selenium Webdriver libraries

## Automation setup to get started
	Git: Simply clone below project and import in Eclipse. voila!! And you are great to go.. 

    git clonehttps://gitlab.com/onkarb68/lightweight-to-do-list-webapp.git
    cd lightweight-to-do-list-webapp
	
# Project overview

	This project gives you a basic project setup, along with some sample tests and supporting classes. 
	The starter project shows , how one can validate, automate any web application to end using selenium libraries

##  Test Scenarios covered for https://todomvc.com/ application to test and automate to-do list web app. Details test step can be seen in cucumber feature file.
	If we would have access to Jira application to author test scenario, test cases , create test cycle to execution. This sounds like a great test artefact one can have. Here, we have used Extent report as test automation artefact for reference. 

	Scenario: Verify user is able to add multiple to-do items in to-do-list
    Given User has navigated to to-do-list page for positive testing
    When User adds multiple ToDoItems in list of to-do-items
      | Gymnasium    |
      | Bath         |
      | Breakfast    |
      | GettingReady |
    Then User should be able to see ToDoItem as one of the item in to-do-list
      | Gymnasium    |
      | Bath         |
      | Breakfast    |
      | GettingReady |
	  
##  Type of Framework: 
	In our project,  have implemented Data-driven Framework by using Page Object Model design pattern with Page Factory.
	  
##  1.Test Base Class: 
	This file deals with all the common functions used by all the page Objects,  step definitions. This file have functions which are wrapper around core selenium(webdriver) commands. These functions would get consumed in all step definitions , page objects file. handles The whole intent of using this file is to have only one file to modify, upgrade, maintain framework seamlessly instead of altering hundreds of file if not done developed properly.
	
	Location: src/main/java/Utility/BaseClass
			........->This class file will have all the frequently used UI actions wrapper in entire selenium framework. The advantage of doing this is easy to maintain single file VS multiple files.
			e.g. In future, if project business demands to automate web app using latest version of selenium libraries or any library, we only need to change this single java file rest all files will remain untouched. This kind of beauty and autonomy this framework brings on the table. 
	
	Note: In current Baseclass, remaining Selenium wrappers can be developed as a ongoing process when respective functionality needs new wrappers to be developed for its automation. Currently have developed only those wrapper which are sufficient for demonstration purposes. 

##  2.How & Where to write feature files?
	Location:  ./Features/PositiveScenariosToDoList.feature
	
	Gherkin uses a set of special keywords to give structure and meaning to executable specifications. 
	Keywords:
		The primary keywords are:

			1.Feature
			2.Given, When, Then, And, But for steps (or *)
			3.Background
			4.Scenario Outline (or Scenario Template)
			5.Examples (or Scenarios)
			
		There are a few secondary keywords as well:

			1.""" (Doc Strings)
			2.| (Data Tables)
			3.@ (Tags)
			4.# (Comments)
	Example:
	Scenario: Verify user is able to add & complete multiple of to-do items in to-do-list
    Given User has navigated to to-do-list page for positive testing
    When User adds multiple ToDoItems in list of to-do-items & complets all the to-do-lists tasks one by one
      | Gymnasium    |
      | Bath         |
      | Breakfast    |
      | GettingReady |
    Then Verify total to-do-list item count gets decremented by one each time when user single complets task

	  
##  3.Where to write step definition? 
		The glue code is responsible for orchestrating calls to a layer of more business-focused classes, which perform the actual REST calls.
	
	Location of file: src/test/java/StepDefination/ToDoListPositiveStepDefinition.java

##  4.POM:How to group reusable feature page specific components?
	Location: src/main/java/PageObject/Homepage
	These Page Object simply models these as objects within the test code. This reduces the amount of duplicated code and means that if the UI changes, the fix need only be applied in one place.
	The functions , elements designed in page objects file are using functions from BaseClass.java for better maintainability of QA automation suite. 
		
	These methods e.g.enterTodoListItem(String itemText) use internally consuming core http calls from BaseClass.java, various reusable methods Utility. This will have reusable UI action specific components or methods which can be consumed anywhere, which helps to make Automation code more modular.
	E.g. Lets consider "enterTodoListItem(String itemText)" method. In case, its implementation gets changed in application. We need to only change in single file, single method, so your entire automation code will not get impacted. This is the great modularity it provides.
	
	As per the Page Object Model, have maintained a class for every web page. Each web page has a separate class and that class holds the functionality and members of that web page. Separate classes for every individual test.
	
	// This method creates to-do items in list
	public void enterTodoListItem(String itemText) {
		click(inputTextBox);
		sendText(inputTextBox, itemText);
		waitForContent();
		pressEnterKey(inputTextBox);
	}

##  5.Data driven testing:	
	Location: src/test/java/TestData/HomepageData
	
	To store the standard data for data driven testing, TestData package is used. We can create page wise classes to store respective data.
	Apart from that have used Scenario outline & Data tables for data driven testing. 
	
	 Scenario: Verify user is able to delete multiple to-do items from to-do-list without completing it.
    Given User has navigated to to-do-list page for positive testing
    When User adds multiple ToDoItems in list of to-do-items & deletes all the to-do-lists tasks one by one
      | Gymnasium    |
      | Bath         |
      | Breakfast    |
      | GettingReady |
    Then All added items should get deleted

	  
##  6.Packages: 
	Have separate packages for Pages and Tests. All the web page related classes & framework level reusable utility comes under the src/main/java package and all the tests related classes come under src/test/java package.

##  7.Properties file:
	This file (browser.peoperties) stores the information that remains static throughout the framework such as browser URL, browser Name etc.

	All the details which change as per the environment and authorization such as URL, Login Credentials are kept in the browser.properties file. Keeping these details in a separate file makes it easy to maintain.

##  8.Hooks:
	Location: src/main/java/Utility/Hooks
	Hooks are blocks of code that can run at various points in the Cucumber execution cycle. They are typically used for setup and teardown of the environment before and after each scenario
	Have handled driver instantiation, loading capabilities etc in Before hook & taking screenshot ,tear down in after hook.
	
##  9.Cucumber Runner:
	Location: src/main/java/TestRunner/Testrunner
	Cucumber is a JUnit extension. It is launched by running JUnit from your build tool or your IDE.
	
##  10.How to generate Test execution  artifacts , reports?
	
	This is again an awesome plugin that is built on Extent Report specially for Cucumber along with Extent report.
	
		Location:  ./ExtentReports/cucumber-reports/report.html
		
##  11.Logger(Log4j):
		Implemented Logger(log4j library) for quick debugging and RCA purpose
		Location:demoApplication.log

##  12.Gitlab CI runner Pipeline
	
	GitLab CI (Continuous Integration) service is a part of GitLab that build and test the software whenever developer pushes code to application.
	So whenever, Git push events happens, implicitly gitlab CI pipeline job gets triggered which would decide Pull request merge based on it pipelines execution status. 

	.gitlab-ci.yml
	
		demo_job_1:
     tags:
       - ci
     script:
      - mvn clean verify

##	13.gitignore file
	Have modified. gitignore file to ignore unnecessary files from reports folder
	
## 14. Defects Found:
	3rd scenario from NegativeScenariosToDoList.feature file is defect, so its failing. This is expected failure as user should not & will not create exactly same to-do-list again. If he tries to create same, then popup should be displayed as "You have already created exactly same item, please check it".

## 15. Naming convention:
	camelCase is a naming convention used for variable , method writing. 
   	

